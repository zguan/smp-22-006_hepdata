# SMP-22-006_HEPDATA

LINK: https://www.hepdata.net/record/sandbox/1682585007

More details can be found in CADI LINE: SMP-22-006

## Details:

Those ipynb-files are used to prepare the histgrams as the inupt:

https://gitlab.cern.ch/zguan/smp-22-006_hepdata/-/blob/master/prepare_fig2.ipynb

https://gitlab.cern.ch/zguan/smp-22-006_hepdata/-/blob/master/prepare_fig3a.ipynb

https://gitlab.cern.ch/zguan/smp-22-006_hepdata/-/blob/master/prepare_fig3b.ipynb

This file can generate the final submission tar:

https://gitlab.cern.ch/zguan/smp-22-006_hepdata/-/blob/master/submission.tar.gz
